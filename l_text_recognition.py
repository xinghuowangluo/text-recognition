import paddlehub as hub
import cv2
import requests
import json
import base64
import sys,os
from tkinter import * 
from tkinter import scrolledtext 
from tkinter import ttk, messagebox, filedialog

window = Tk()
menubar = Menu(window)
window.title("图片文字识别")
window.geometry("1200x700")
ocr = hub.Module(name="chinese_ocr_db_crnn_server")

scr = scrolledtext.ScrolledText(window, width=80, height=15,font=("隶书",18))  #滚动文本框（宽，高（这里的高应该是以行数为单位），字体样式）
scr.place(x=50, y=50) #滚动文本框在页面的位置
scr.insert(END,"版本v0.0.2\n\n")   #将错题显示在动文本框中

def ocr_img(scr=scr):
	scr.insert(END,"图片识别中。。。。。\n\n")
	# 发送HTTP请求
	img_filename = filedialog.askopenfilename(title=u'选择文件', initialdir=(os.path.expanduser("d://imgtofont")))
	scr.insert(END,img_filename+"\n\n")
	result = ocr.recognize_text(images=[cv2.imread(img_filename)])
	for ra in result:
		for line in ra['data']:
			# print(line['text'])
			scr.insert(END,line['text']+"\n")

	scr.insert(END,"\n////////////////////////////\n")
	scr.insert(END,"图片识别完成！\n\n")

button2 = Button(window, text="开始识别", font=("隶书", 10), command=ocr_img)
button2.place(x=500, y=600, width=150, height=60)
button3 = Button(window, text="退	出", font=("隶书", 10), command=sys.exit)
button3.place(x=700, y=600, width=150, height=60)
window.mainloop()
exit()