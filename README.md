# TextRecognition

#### 介绍
本地文字识别客户端脚本

#### 软件架构
软件架构说明


#### 安装教程
一，linux 服务器端部署
	1.  安装paddlepub <br>
		pip install --upgrade paddlepaddle -i https://mirror.baidu.com/pypi/simple <br>
		pip install --upgrade paddlehub -i https://mirror.baidu.com/pypi/simple <br>
		
	2. 安装最新版本的模型库 <br>
		hub install chinese_ocr_db_crnn_server <br>
	3. 启动服务 <br>
		hub serving start -m chinese_ocr_db_crnn_server <br>
	
二，客户端 <br> 
	1. 设置服务器地址 <br>
	2. 安装依赖 可参考paddlehub chinese_ocr_db_crnn_server模型。 <br>
		pip install shapely <br>
		pip install pyclipper <br>

	3. 可使用pyinstaller.exe -F text_recognition.py 打包成二进制文件 .

三，windows客户端下载地址<br>

	链接: https://pan.baidu.com/s/1wMNnf5btZNk75XbCKZq8cQ  密码: ahpd

#### 使用说明

    

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
